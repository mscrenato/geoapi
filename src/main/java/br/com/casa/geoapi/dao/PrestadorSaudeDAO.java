package br.com.casa.geoapi.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.casa.geoapi.model.PrestadorSaude;

@Component
public class PrestadorSaudeDAO {
	
	private Optional<List<PrestadorSaude>> data;
	
	
	public PrestadorSaudeDAO() {
		new PrestadorSaude(1, "", "", new String[]{"",""});
		data = Optional.of(Arrays.asList(
				new PrestadorSaude(1,"Prestador 1","Av. Anápolis, 100 - Bethaville I",new String[]{"Cardiologia","Neurologia","Neurocirurgia"}),
				new PrestadorSaude(2,"Prestador 2","Av. Juruá - Alphaville Industrial",new String[]{"Clínica Geral","Pediatria"}),
				new PrestadorSaude(3,"Prestador 3","Av. Cauaxi, 293 - Alphaville Industrial",new String[]{"Cardiologia","Dermatologia"}),
				new PrestadorSaude(4,"Prestador 4","Av. Vinte e Seis de Março, 236",new String[]{"Dermatologia","Alergista"}),
				new PrestadorSaude(5,"Prestador 5","Av. Hildebrando de Lima, 610 - Km 18",new String[]{"Endrocrinologia","Nutrologia"}),
				new PrestadorSaude(6,"Prestador 6","R. Gasparino Lunardi, 115 - Jardim das Flores",new String[]{"Dermatologia"}),
				new PrestadorSaude(7,"Prestador 7","Av. Santo Antônio, 1997",new String[]{"Urologia","Vascular"}),
				new PrestadorSaude(8,"Prestador 8","Avenida dos Autonomistas, 1074",new String[]{"Tricologia","Otorrinolaringologia"}),
				new PrestadorSaude(9,"Prestador 9","Av. Hilário Pereira de Souza, 492 - Industrial Autonomistas",new String[]{"Geriatria"}),
				new PrestadorSaude(10,"Prestador 10","R. da Consolação, 1414 - Consolação",new String[]{"Ginecologia"}),
				new PrestadorSaude(11,"Prestador 11","Av. Paulo VI - Sumaré",new String[]{"Vascular"}),
				new PrestadorSaude(12,"Prestador 12","Av. Brg. Faria Lima, 1355 - Jardim Paulistano",new String[]{"Psicologia","Psiquiatria"}),
				new PrestadorSaude(13,"Prestador 13","Av. Eusébio Matoso - Pinheiros",new String[]{"Mastologia","Ginecologia"}),
				new PrestadorSaude(14,"Prestador 14","R. Olimpíadas - Itaim Bibi",new String[]{"Coloproctologia"}),
				new PrestadorSaude(15,"Prestador 15","Av. Santo Amaro, 699 - Vila Nova Conceição",new String[]{"Cardiologia"}),
				new PrestadorSaude(16,"Prestador 16","R. Agostinho Gomes, 1928 - Ipiranga",new String[]{"Fonoaudiologia"}),
				new PrestadorSaude(17,"Prestador 17","Av. Tâmara, 635 - Centro",new String[]{"Vascular","Tricologia"}),
				new PrestadorSaude(18,"Prestador 18","Av. Alphaville, 984 - Empresarial 18 do Forte",new String[]{"Dermatologia","Pediatria"}),
				new PrestadorSaude(19,"Prestador 19","Av. Alphaville, 1138 - Empresarial 18 do Forte",new String[]{"Nutrologia","Geriatria"}),
				new PrestadorSaude(20,"Prestador 20","Av. Piracema, 1341 - Tamboré",new String[]{"Clínica Geral"})
				));
	}
	
	public Optional<List<PrestadorSaude>> getPrestadoresSaude() {
		return data;
	}

	public Optional<List<PrestadorSaude>> getPrestadoresPorEspecialidade(String especialidade) {
		if (data.isPresent()) {
			return Optional.of(data.get().stream().filter((PrestadorSaude e) -> {
				return Arrays.asList(e.getEspecialidades()).contains(especialidade);
			}).collect(Collectors.toList()));
		}
		return data;
	}
}
