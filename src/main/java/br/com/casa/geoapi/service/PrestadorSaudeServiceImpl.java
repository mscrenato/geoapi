package br.com.casa.geoapi.service;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;

import br.com.casa.geoapi.connection.GoogleAPI;
import br.com.casa.geoapi.dao.PrestadorSaudeDAO;
import br.com.casa.geoapi.model.PrestadorSaude;

@Service
public class PrestadorSaudeServiceImpl implements IPrestadorSaudeService {

	@Autowired
	private PrestadorSaudeDAO dao;
	@Autowired
	private GoogleAPI api;
	
	@Override
	public Optional<List<PrestadorSaude>> getPrestadoresSaude(){
		return dao.getPrestadoresSaude();
	}
	
	@Override
	public Optional<List<PrestadorSaude>> getPrestadoresSaudePorEspecialidade(String especialidade) {
		return dao.getPrestadoresPorEspecialidade(especialidade);
	}

	@Override
	public Double calcularDistancia(double lat1, double lon1, double lat2, double lon2) {
		DecimalFormat df = new DecimalFormat(".##");
		final int R = 6371; 

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
		+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
		* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c; 
		distance = Double.valueOf(df.format(Math.pow(distance, 2) + Math.pow(0.0, 2)));
		return Math.sqrt(distance);
	}


	@Override
	public LatLng getPrestadorSaudeLatLng(String endereco) throws ApiException, InterruptedException, IOException {
		return api.getLatLngDeUmEndereco(endereco);
	}

	@Override
	public boolean isLatLngValidos(double lat, double lng) {
		return (lat >= -90.0 && lat <= 90.0) && (lng >= -180.0 && lng <= 180.0);
	}

}
