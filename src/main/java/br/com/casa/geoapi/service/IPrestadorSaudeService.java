package br.com.casa.geoapi.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;

import br.com.casa.geoapi.model.PrestadorSaude;

public interface IPrestadorSaudeService {
	
	Optional<List<PrestadorSaude>> getPrestadoresSaude();
	Optional<List<PrestadorSaude>> getPrestadoresSaudePorEspecialidade(String especialidade);
	Double calcularDistancia(double lat1, double lon1, double lat2,double lon2);
	LatLng getPrestadorSaudeLatLng(String endereco) throws ApiException, InterruptedException, IOException;
	boolean isLatLngValidos(double lat, double lng);
	
}
