package br.com.casa.geoapi.connection;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

@Component
public class GoogleAPI {
	private static final String GOOGLE_API_KEY = "AIzaSyC4DO-51BrYRqXiJi9KLbOasIP4T8u_bME";
	private GeoApiContext context = new GeoApiContext.Builder().apiKey(GOOGLE_API_KEY).build();

	public Double calcularDistancia(double lat1, double lon1, double lat2, double lon2) {
		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
		+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
		* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c; 

		return Math.sqrt(distance);
	}
	
	public LatLng getLatLngDeUmEndereco(String endereco) throws ApiException, InterruptedException, IOException {
		GeocodingResult[] results = GeocodingApi.geocode(context, endereco).await();
		return results[0].geometry.location;
	}
}
