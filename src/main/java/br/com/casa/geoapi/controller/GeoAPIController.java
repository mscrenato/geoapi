package br.com.casa.geoapi.controller;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;

import br.com.casa.geoapi.model.PrestadorSaude;
import br.com.casa.geoapi.model.PrestadorSaudeDTO;
import br.com.casa.geoapi.service.IPrestadorSaudeService;

@RestController
@RequestMapping(value="api")
public class GeoAPIController {
	
	private static final Logger LOGGER = Logger.getLogger( GeoAPIController.class.getName() );
	
	@Autowired
	private IPrestadorSaudeService service;
	
	@GetMapping(value="/obterPrestadoresSaude", produces = "application/json")
	ResponseEntity<Object> obterPrestadoresSaude(@RequestParam double latitude, @RequestParam double longitude, @RequestParam String especialidade){
		if(!service.isLatLngValidos(latitude, longitude)) {
			return new ResponseEntity<>("Latitude e/ou Longitude inválidos",HttpStatus.BAD_REQUEST);
		}
		Optional<List<PrestadorSaude>> lista =  service.getPrestadoresSaudePorEspecialidade(especialidade);
		Set<PrestadorSaudeDTO> saudeDTOs = new HashSet<>();
		if(lista.isPresent() && !lista.get().isEmpty()) {
			Consumer<PrestadorSaude> consumer = (PrestadorSaude p) -> {
				try {
					LatLng latLng = service.getPrestadorSaudeLatLng(p.getEndereco());
					saudeDTOs.add(new PrestadorSaudeDTO(
							p.getNome(), 
							p.getEndereco(), 
							latLng.lat, 
							latLng.lng, 
							0.0));
				} catch (ApiException | IOException e) {
					LOGGER.log(Level.SEVERE, e.toString(), e);
				} catch (InterruptedException e1) {
					LOGGER.log(Level.SEVERE, e1.toString(), e1);
					Thread.currentThread().interrupt();
				}
			};
			lista.get().forEach(consumer);
			saudeDTOs.forEach(e -> {
				double dist = service.calcularDistancia(latitude, longitude, e.getLatitude(), e.getLongitude());
				e.setDistanciaEmKm(dist);
			});
			return new ResponseEntity<>(saudeDTOs.stream().sorted(Comparator.comparingDouble(PrestadorSaudeDTO::getDistanciaEmKm)).collect(Collectors.toList()),HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
