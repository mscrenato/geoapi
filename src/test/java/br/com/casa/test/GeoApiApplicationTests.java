package br.com.casa.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import br.com.casa.geoapi.service.IPrestadorSaudeService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GeoApiApplicationTests {
	
	@Autowired
	private IPrestadorSaudeService service;

	@Test
	public void testLatitudeLongitudeValidosSucesso() {
		double lat = 89.0;
		double lng = -10.0;
		
		assertThat(service.isLatLngValidos(lat, lng)).isTrue();
	}
	
	@Test
	public void testLatitudeLongitudeValidosZeroLatZeroLngSucesso() {
		double lat = 0.0;
		double lng = 0.0;
		
		assertThat(service.isLatLngValidos(lat, lng)).isTrue();
	}
	
	@Test
	public void testLatitudeLongitudeValidosLatInvalidoFalso() {
		double lat = 91.0;
		double lng = 0.0;
		
		assertThat(service.isLatLngValidos(lat, lng)).isFalse();
	}
	
	@Test
	public void testLatitudeLongitudeValidosLngInvalidoFalso() {
		double lat = 0.0;
		double lng = 200.0;
		
		assertThat(service.isLatLngValidos(lat, lng)).isFalse();
	}
	
	@Test
	public void testLatitudeLongitudeValidosLatLngLimitesSucesso() {
		double lat = 90.0;
		double lng = 180.0;
		
		assertThat(service.isLatLngValidos(lat, lng)).isTrue();
	}
	
	@Test
	public void testLatitudeLongitudeValidosLatLngLimitesFalse() {
		double lat = -90.1;
		double lng = -180.1;
		
		assertThat(service.isLatLngValidos(lat, lng)).isFalse();
	}
}
